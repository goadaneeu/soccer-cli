FROM python:2.7

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . ./

RUN python setup.py install

#CMD [ "python", "" ]
